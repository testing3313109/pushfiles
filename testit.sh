# Function to display an error message dialog

show_error_dialog() {
    zenity --error --text="$1" --title="Error"
}

# Function to prompt for GitLab username
get_gitlab_username() {
    GITLAB_USERNAME=$(zenity --entry --text="Enter your GitLab username:" --title="GitLab Username")
    if [ -z "$GITLAB_USERNAME" ]; then
        show_error_dialog "GitLab username cannot be empty. Aborting."
        exit 1
    fi
}

# Function to prompt for GitLab access token
get_gitlab_token() {
    GITLAB_TOKEN=$(zenity --password --text="Enter your GitLab password:" --title="GitLab Access Token")
    if [ -z "$GITLAB_TOKEN" ]; then
        show_error_dialog "GitLab access token cannot be empty. Aborting."
        exit 1
    fi
}

# Function to prompt for file to push
get_file_to_push() {
    FILE_TO_PUSH=$(zenity --file-selection --title="Select File to Push")
    if [ -z "$FILE_TO_PUSH" ]; then
        show_error_dialog "No file selected. Aborting."
        exit 1
    fi
}

# Function to push file to GitLab
push_to_gitlab() {
    # Clone the repository
    git clone "$GITLAB_REPO_URL" tmp_repo || { show_error_dialog "Failed to clone repository. Check if the repository URL is correct and try again."; exit 1; }

    # Move to the cloned repository directory
    cd tmp_repo

    # Configure GitLab user
    git config user.name "$GITLAB_USERNAME"
    git config user.email "$GITLAB_USERNAME@example.com"

    # Copy the file to the repository directory
    cp "$FILE_TO_PUSH" .

    # Add the file to the staging area
    git add .

    # Commit the changes
    git commit -m "Add $FILE_TO_PUSH"

    # Push the changes to GitLab
    git push origin master || { show_error_dialog "Failed to push changes to GitLab. Make sure you have permissions and try again."; exit 1; }

    # Clean up
    cd ..
    rm -rf tmp_repo

    zenity --info --text="File pushed successfully to GitLab repository." --title="Success"
}

# GitLab repository URL
GITLAB_REPO_URL="https://gitlab.com/testing3313109/Pilot.git"

# Prompt for GitLab credentials
get_gitlab_username
get_gitlab_token

# Prompt for file to push
get_file_to_push

# Push file to GitLab
push_to_gitlab
