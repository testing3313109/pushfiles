#!/bin/bash

# Set the URL of the Git repository
repository_url="https://gitlab.hrg.systems/henri.gerstadt/control_robot_parameters.git"

# Set the destination directory where the repository will be cloned
destination_folder="$HOME/Desktop/control_robot_parameters"

# Clone the repository
git clone "$repository_url" "$destination_folder"

# Check if Git is installed
if ! command -v git &> /dev/null; then
    echo "Git is not installed. Please install Git first."
    exit 1
fi

# Create a new folder on the Desktop
echo "Enter the name for the new folder:"
read folder_name

new_folder="$HOME/Desktop/$folder_name"
mkdir "$new_folder"

# Ask for the type of robot
echo "Enter the type of robot (maira7S, maira7M, maira7L, maira6M, maira6S or maira6L):"
read robot_type

# Ask for the person name
echo "Enter your Name:"
read Name

# Copy files based on the robot type
if [ "$robot_type" = "${robot_type}" ]; then
    # Copy files for ${robot_type}
    cp "/home/hrg/.local/config/robot_parameters/kinematic_parameters_${robot_type}.param" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/dynamic_parameters_${robot_type}.param" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/friction_parameters_${robot_type}.json" "$new_folder"
    cp "/home/hrg/.local/config/dynamic_identification/measurements/_${robot_type}.csv" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/collision_parameters/collsion_parameters_${robot_type}.json" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/robot_config/control_parameters_${robot_type}.json" "$new_folder"
elif [ "$robot_type" = "maira7Lh" ]; then
    # Copy files for maira7Ll
   cp "/home/hrg/.local/config/robot_parameters/kinematic_parameters_${robot_type}.param" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/dynamic_parameters_${robot_type}.param" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/friction_parameters_${robot_type}.json" "$new_folder"
    cp "/home/hrg/.local/config/dynamic_identification/measurements/${robot_type}.csv" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/collision_parameters/collsion_parameters_${robot_type}.json" "$new_folder"
    cp "/home/hrg/.local/config/robot_parameters/robot_config/control_parameters_${robot_type}.json" "$new_folder"
else
    echo "No files copied. Invalid robot type."
    exit 1
fi

# Copy the folder to the cloned repository
if [ "$robot_type" = "${robot_type}" ]; then
    cloned_folder="$destination_folder/serial_production/maira/${robot_type}"
elif [ "$robot_type" = "maira7Ll" ]; then
    cloned_folder="$destination_folder/serial_production/maira/maira7L"
fi

cp -r "$new_folder" "$cloned_folder"

# Commit and push the changes to the repository
cd "$destination_folder"
git add .
git commit -m "Added by ${Name}"
git push

echo "Files copied and committed to the repository successfully."
