import ast
from pymongo import MongoClient
import requests
import json
client = MongoClient('localhost', 27017)

client.drop_database('mira')
url = 'http://localhost:8081/api/'
flag = False
# 70
# users
data = requests.post(url+'users/register_basicuser', json={
    'name': 'Basic User',
    'password': 'basicuser',
    'description': 'Limited Features',
    'type': 'Guest'
}).json()

if (requests.post(url+'users/register_superadmin', json={
    'name': 'SuperAdmin',
    'password': 'neura1',
    'description': 'More Advanced Features Available',
    'type': 'SuperAdmin'
}).status_code != 201):
    print('Error please check with server, user admin')
    flag = True
# cartesianjointfreedrive
header = {'Authorization': data['token']}
voice_pick_response = requests.post(url+'treeprogramlist', json={
    "description": "This program auto generates a voice pick",
    "name": "NR_Voice_Pick",
    "isCompleted": True
}, headers=header)
if (voice_pick_response.status_code != 201):
    print('Error please check with server, voice pick list ')
    flag = True
else:
    print('Successfully created with 200 -  voice pick')

    with open('pick_program.json') as json_file:
        data = json.load(json_file)
        data['pid'] = voice_pick_response.json()["_id"]
        if (requests.post(url+'treeprogramdata', json=data, headers=header).status_code != 201):
            print('Error please check with server, pick program')
            flag = True
        else:
            print('Successfully created with 200 -  pick program')

tool_response = requests.post(url+'tool', json={
    'name': 'NoTool',
    'description': 'Tool Description',
    'robot_type': 'Tool',
    'offsetX': 0,
    'offsetY': 0,
    'offsetZ': 0,
    'offsetA': 0,
    'offsetB': 0,
    'offsetC': 0,
    'autoM': 0,
    'inertiaXX': 0,
    'inertiaXY': 0,
    'inertiaYY': 0,
    'inertiaYZ': 0,
    'inertiaXZ': 0,
    'inertiaZZ': 0,
    'autoMeasureX': 0,
    'autoMeasureY': 0,
    'autoMeasureZ': 0,
    '_toolOA': False,
    '_toolOD': False,
    '_controlOA': False,
    '_controlOD': False,
    'closeInput': 0,
    'openInput': 40,
    'grippertype': 'Standard Gripper',
    'speed': 0,
    'force': 0,

    'onTOD': 0,

    'onTOA': [0, 0],

    'onCOA': [0, 0, 0, 0, 0, 0, 0, 0],

    'onCOD1': 0,

    'onCOD2': 0,

    'offTOD': 0,

    'offTOA': [0, 0],

    'offCOA': [0, 0, 0, 0, 0, 0, 0, 0],

    'offCOD1': 0,

    'offCOD2': 0,

    'palmDiameter':  0.1,
    'palmThickness':  0.02,
    'palmLength':  0.2,
    'fingerDiameter':  0.01,
    'fingerThickness':  0.02,
    'fingerLength':  0.035,
    'minFingerOpening':  0.04,
    'maxFingerOpening':  0.055,
    'defaultFingerOpening':  0.07
})
if (tool_response.status_code != 201):
    print('Error please check with server, notool')
    flag = True
else:
    print('Successfully created with 200 -  notool')

""" 
robotiq_response = requests.post(url+'tool', json={
    'name': 'RobotiQ',
    'description': 'Tool Description',
    'robot_type': 'Tool',
    'offsetX': 0,
    'offsetY': 0,
    'offsetZ': 0.245,
    'offsetA': 0,
    'offsetB': 0,
    'offsetC': 0,
    'autoM': 1.2,
    'inertiaXX': 0,
    'inertiaXY': 0,
    'inertiaYY': 0,
    'inertiaYZ': 0,
    'inertiaXZ': 0,
    'inertiaZZ': 0,
    'autoMeasureX': 0,
    'autoMeasureY': 0,
    'autoMeasureZ': 0.08,
    '_toolOA': False,
    '_toolOD': False,
    '_controlOA': False,
    '_controlOD': False,
    'closeInput': 0.04,
    'openInput': 0.04,
    'grippertype': 'Modbus Gripper',
    'speed': 50,
    'force': 50,

    'onTOD': 0,

    'onTOA': [0, 0],

    'onCOA': [0, 0, 0, 0, 0, 0, 0, 0],

    'onCOD1': 0,

    'onCOD2': 0,

    'offTOD': 0,

    'offTOA': [0, 0],

    'offCOA': [0, 0, 0, 0, 0, 0, 0, 0],

    'offCOD1': 0,

    'offCOD2': 0,

    'palmDiameter':  0.1,
    'palmThickness':  0.02,
    'palmLength':  0.2,
    'fingerDiameter':  0.01,
    'fingerThickness':  0.02,
    'fingerLength':  0.035,
    'minFingerOpening':  0.04,
    'maxFingerOpening':  0.055,
    'defaultFingerOpening':  0.07
})
if (robotiq_response.status_code != 201):
    print('Error please check with server, robotiq')
    flag = True
else:
    print('Successfully created with 200 -  robotiq') """

# tool

if (requests.post(url+'selectedtool', json={
    "id": tool_response.json()["_id"],
    "name": tool_response.json()["name"]
}, headers=header).status_code != 201):
    print('Error please check with selectedtool')
    flag = True
else:
    print('Successfully created with 200 -  selectedtool')

if (requests.post(url+'cartesian', json={
    "SelectedTool.id": tool_response.json()["_id"],
    "SelectedTool.Name": tool_response.json()["name"]
}, headers=header).status_code != 201):
    print('Error please check with server cartesian')
    flag = True
else:
    print('Successfully created with 200 -  cartesian')


if (requests.post(url+'point', json={
    "type": "Point",
    "visibility": True,
    "description": "test",
    "originX": 0.257,
    "originY": 0.0,
    "originZ": 0.580,
    "originA": -3.14159,
    "originB": 0.0,
    "originC": -1.5707963,
    "offsetX": 0.0,
    "offsetY": 0.0,
    "offsetZ": 0.0,
    "offsetA": 0.0,
    "offsetB": 0.0,
    "offsetC": 0.0,
    "a1": 0.0,
    "a2": -0.5235987766,
    "a3": 0.0,
    "a4": 2.61624854,
    "a5": 0.0,
    "a6": 1.047197551,
    "a7": -1.5707963,
    "name": "Parking"
}, headers=header).status_code != 201):
    print('Error please check with server point Parking')
    flag = True
else:
    print('Successfully created with 200 -  point Parking')


if (requests.post(url+'point', json={
    "type": "Point",
    "visibility": True,
    "description": "test",
    "originX": 0.7,
    "originY": 0.0,
    "originZ": 1.023,
    "originA": 3.14159,
    "originB": 0.0,
    "originC": 3.14159,
    "offsetX": 0.0,
    "offsetY": 0.0,
    "offsetZ": 0.0,
    "offsetA": 0.0,
    "offsetB": 0.0,
    "offsetC": 0.0,
    "a1": 0.0,
    "a2": 0.0,
    "a3": 0.0,
    "a4": 1.5707963,
    "a5": 0.0,
    "a6": 1.5707963,
    "a7": 0.0,
    "name": "Home"
}, headers=header).status_code != 201):
    print('Error please check with server point HOME POSITION')
    flag = True
else:
    print('Successfully created with 200 -  point HOME POSITION')


if (requests.post(url+'joint', headers=header).status_code != 201):
    print('Error please check with server, joint')
    flag = True
else:
    print('Successfully created with 200 -  joint')


if (requests.post(url+'freedrive', headers=header).status_code != 201):
    print('Error please check with server freedrive')
    flag = True
else:
    print('Successfully created with 200 -  freedrive')

if (requests.post(url+'zerogaxislock', json={
    "cartesian": [False, False, False, False, False, False],
    "joint": [False, False, False, False, False, False, False],
    "reference_frame": "base"
}, headers=header).status_code != 201):
    print('Error please check with server zerogaxislock')
    flag = True
else:
    print('Successfully created with 200 -  zerogaxislock')
# 3

# IO
# controllerinputdigital 16

for x in range(0, 16):
    if (requests.post(url+'cabinpdigital', json={
        "UniqueID": "1."+str(x),
        "Name": "DI_"+str(x),
        "UniqueName": "DI_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with server cabinpdigital')
        flag = True
    else:
        print('Successfully created with 200 -  cabinpdigital')
# 19
# toolinputanalog 2

for x in range(0, 2):
    if (requests.post(url+"toolinpanalog", json={
        "UniqueID": "1."+str(x),
        "Name": "TIA_"+str(x),
        "UniqueName": "TIA_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with toolinpanalog')
        flag = True
    else:
        print('Successfully created with 200 -  toolinpanalog')
# 21
# toolinputdigital 3

for x in range(0, 3):
    if (requests.post(url+"toolinpdigital", json={
        "UniqueID": "1."+str(x),
        "Name": "TID_"+str(x),
        "UniqueName": "TID_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with server toolinpdigital')
        flag = True
    else:
        print('Successfully created with 200 -  toolinpdigital')
# 24
# controlleroutputdigital 16

for x in range(0, 16):
    if (requests.post(url+"caboutpdigital", json={
        "UniqueID": "1."+str(x),
        "Name": "DO_"+str(x),
        "UniqueName": "DO_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with caboutpdigital')
        flag = True
    else:
        print('Successfully created with 200 -  caboutpdigital')
# 40
# controller output analog 8

for x in range(0, 8):
    if (requests.post(url+"caboutpanalog", json={
        "UniqueID": "1."+str(x),
        "Name": "AO_"+str(x),
        "UniqueName": "AO_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with caboutpanalog')
        flag = True
    else:
        print('Successfully created with 200 -  caboutpanalog')

# 48
# controller input analog 8

for x in range(0, 8):
    if (requests.post(url+"cabinpanalog", json={
        "UniqueID": "1."+str(x),
        "Name": "AI_"+str(x),
        "UniqueName": "AI_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with cabinpanalog')
        flag = True
    else:
        print('Successfully created with 200 -  cabinpanalog')
# 56
# tool output digital 3

for x in range(0, 3):
    if (requests.post(url+"tooloutpdigital", json={
        "UniqueID": "1."+str(x),
        "Name": "TOD_"+str(x),
        "UniqueName": "TOD_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with tooloutpdigital')
        flag = True
    else:
        print('Successfully created with 200 -  tooloutpdigital')
# 59
# tool output analog 2

for x in range(0, 2):
    if (requests.post(url+"tooloutpanalog", json={
        "UniqueID": "1."+str(x),
        "Name": "TOA_"+str(x),
        "UniqueName": "TOA_"+str(x)
    }, headers=header).status_code != 201):
        print('Error please check with tooloutpanalog')
        flag = True
    else:
        print('Successfully created with 200 -  tooloutpanalog')
# 61
# mode-state-vgs-button
if (requests.post(url+"launch", headers=header).status_code != 201):
    print('Error please check with server')
    flag = True

if (requests.post(url+"log_", json={
    "results": [
        {
            "data": "Info : Sample"

        }
    ]
}, headers=header).status_code != 201):
    print('Error please check with launch')
    flag = True
else:
    print('Successfully created with 200 -  launch')

if (requests.post(url+'vgs', headers=header).status_code != 201):
    print('Error please check with vgs')
    flag = True
else:
    print('Successfully created with 200 -  vgs')

if (requests.post(url+'hmibutton', headers=header).status_code != 201):
    print('Error please check with hmibutton')
    flag = True
else:
    print('Successfully created with 200 -  hmibutton')
# 65
# Manual joint cartesian
if (requests.post(url+'mancart', headers=header).status_code != 201):
    print('Error please check with mancart')
    flag = True
else:
    print('Successfully created with 200 -  mancart')
if (requests.post(url+'manjoint', headers=header).status_code != 201):
    print('Error please check with manjoint')
    flag = True
else:
    print('Successfully created with 200 -  manjoint')
# 67
# jointangles cartesianposes encoder
if (requests.post(url+'jointangles', headers=header).status_code != 201):
    print('Error please check with jointangles')
    flag = True
else:
    print('Successfully created with 200 -  jointangles')

if (requests.post(url+'encoderoffset', headers=header).status_code != 201):
    print('Error please check with encoderoffset')
    flag = True
else:
    print('Successfully created with 200 -  encoderoffset')

if (requests.post(url+'cartesianpose', headers=header).status_code != 201):
    print('Error please check with cartesianpose')
    flag = True
else:
    print('Successfully created with 200 -  cartesianpose')

gravity_vector = requests.post(url+'gravityvector', json={
    "gravity_vector_x": 0,
    "gravity_vector_y": 0,
    "gravity_vector_z": -9.8,
})
if (gravity_vector.status_code != 201):
    print('Error please check with server, gravityvector')
    flag = True
else:
    print('Successfully created with 200 -  gravityvector')

if(requests.post(url+'cameracalibration', json={
    "name": "Wrist",
    "description": "test",
    "camera_topic": "/camera/color/image_rect_color",
    "camera_info_topic": "/camera/color/camera_info",
    "camera_frame": "camera_color_optical_frame",
    "end_effector_frame": "camera_calibration_link",
    "base_frame": "root_link",
    "marker_frame": "marker_frame",
    "solver_name": "john_doe",
    "sensor_configuration": 0,
    "use_opencv_handeye": False,
    "pose_sample_file": "handeye_pose_file.json"
    
}).status_code != 201):
    print('Error Wrist Camera')
    flag = True
else:
    print('Successfully created with 200 -  Wrist Camera')

if (flag == False):
    print('Successful, <201>')
else:
    print("please check for 404 or 400 errrors")
